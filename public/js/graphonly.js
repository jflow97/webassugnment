google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback(drawDaily);

/*function drawDaily() {

      var dataArr = document.getElementById('daily_chart').attributes;
      console.log("draw : "+dataArr);
      var data = google.visualization.arrayToDataTable(dataArr);

      var options = {
        title: 'Daily Expenses',
      };

      var chart = new google.visualization.PieChart(document.getElementById('daily_chart'));

      chart.draw(data, options);
}*/

function drawDaily() {

  var dataArr = document.getElementById('noDisplay').innerHTML;
  var dataDate = document.getElementById('noDisplay2').innerHTML;
  var dcheck = false;
  console.log(document.getElementById('noDisplay').innerHTML);
  var longArr = dataArr.split(",");
  var newArr = [];
  var smallArr = [];
  for(var i = 0; i < longArr.length; i++){
    if(smallArr.length < 2){
      if(i>2 && smallArr.length == 1){
        if(parseFloat(longArr[i]) != 0){
          dcheck = true;
        }
        smallArr.push(parseFloat(longArr[i]))
      }
      else{
        smallArr.push(longArr[i]);
      }
    }
    if(smallArr.length == 2){
      newArr.push(smallArr);
      smallArr = [];
    }
  }
  console.log(newArr);
  var data = google.visualization.arrayToDataTable(newArr);

  var options = {
    title: 'Daily Expenses on ' + dataDate,
  };

  var chart = new google.visualization.PieChart(document.getElementById('daily_chart'));

  if(dcheck){
    chart.draw(data, options);
  }
  else{
    document.getElementById('daily_chart').innerHTML = "<br> No transactions on that day!"
  }
}

// ['Category', 'Amount(RM)',],
//     ['Food', 500],
//     ['Social Life', 200],
//     ['Transportation', 100],
//     ['Education', 50],
//     ['Others', 150]

// google.charts.load('current', {packages: ['corechart','bar']});
// google.charts.setOnLoadCallback(drawChart);

// function drawChart() {
//       var data = new google.visualization.DataTable();
//       data.addColumn('string', 'Month');
//       data.addColumn('number', 'Amount(RM)');

//       data.addRows([
//         [{f: 'January'}, 1234],
//         [{f: 'February'}, 234],
//         [{f:'March'}, 345],
//         [{f: 'April'}, 456],
//         [{f: 'May'}, 578],
//         [{f: 'June'}, 623],
//         [{f: 'July'}, 712],
//         [{f: 'August'}, 812],
//         [{f: 'September'}, 923],
//         [{f: 'October'}, 1023],
//         [{f: 'November'}, 1023],
//         [{f: 'December'}, 1012]
//       ]);

//       var options = {
//         title: 'Daily Expenses',
//         hAxis: {
//           title: 'Month',
//           viewWindow: {
//             min: [1, 30, 0],
//             max: [12, 30, 0]
//           }
//         },
//         vAxis: {
//           title: 'Amount(RM)'
//         }
//       };

//       var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));

//       chart.draw(data, options);
// }