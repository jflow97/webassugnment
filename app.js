var express               = require("express"),
    mongoose              = require("mongoose"),
    passport              = require("passport"),
    bodyParser            = require("body-parser"),
    LocalStrategy         = require("passport-local"),
    passportLocalMongoose = require("passport-local-mongoose"),
    nodemailer            = require('nodemailer'),
    User                  = require("./models/user");

//Open a connection to the auth_demo_app database on our locally running instance of MongoDB
mongoose.connect("mongodb://localhost/savemo");

var app = express();
app.set('view engine', 'ejs');
app.use(express.static("public"));

//tell express to use the body-parser, express-session and passport
app.use(bodyParser.urlencoded({extended: true}));
app.use(require("express-session")({
    secret: "Savemo savemo savemomomo ahaha", //used to encode and decode session
    resave: false,                                           
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

//email set up
//use to send email
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'savemo.official@gmail.com',
        pass: 'A12b!@cd'
    }
});

//Telll Passport to use the local strategy
passport.use(new LocalStrategy(User.authenticate()));
//Passport reading the session, taking data from the session and encode and decode it 
passport.serializeUser(User.serializeUser());         //encode 
passport.deserializeUser(User.deserializeUser());     // decode


//============
// ROUTES
//============

//Define root path using render() method
app.get("/", function(req, res){
    res.render("index", {});
});
//Define root path, but this one can get something
app.get("/manage",isLoggedIn, function(req, res){
    res.render("manage", {transArr : req.user.transactions});
});
app.get("/profile",isLoggedIn, function(req, res){
    res.render("profile", {usernameVar: req.user.username, nameVar: req.user.name, budgetVar: req.user.budget, targetVar: req.user.target, ageVar: req.user.age, emailVar: req.user.email});
});
app.get("/profile_home",isLoggedIn, function(req, res){
    if(req.user.transactions == undefined){
        res.render("profile_home", {nameVar: req.user.name, budgetVar: req.user.budget, targetVar: req.user.target, transArr: 0 });
    }else
    res.render("profile_home", {nameVar: req.user.name, budgetVar: req.user.budget, targetVar: req.user.target, transArr: req.user.transactions });
});
app.get("/report",isLoggedIn, function(req, res){
    if(req.user.transactions == undefined){
        res.render("report", {nameVar: req.user.name, budgetVar: req.user.budget, targetVar: req.user.target, transArr: 0, 
            dataArr: [
                 ['Category', 'Amount(RM)',],
                 ['Food', 0],
                 ['Social Life', 0],
                 ['Transportation', 0],
                 ['Education', 0],
                 ['Others', 0]
                ] });
    }
    else{
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd = '0'+dd
        } 
        if(mm<10) {
            mm = '0'+mm
        } 
        today =yyyy + '-' + mm + '-' + dd;
        console.log("date "+ today);
        var tempList = [];
        req.user.transactions.forEach(function(t){
            if(t.date == today){
                tempList.push(t);
            }
        })
        console.log("list "+tempList);
        var n1 = 0,
            n2 = 0,
            n3 = 0,
            n4 = 0,
            n5 = 0;
        tempList.forEach(function(trans){
            console.log("trans.cat : "+trans.categories);
            if (trans.categories == "Food"){
                n1 = n1 + trans.amount; 
            }
            else if(trans.categories == "Social Life"){
                n2 = n2 + trans.amount
            }
            else if(trans.categories == "Transportation"){
                n3 = n3 + trans.amount
            }
            else if(trans.categories == "Education"){
                n4 = n4 + trans.amount
            }
            else {
                n5 = n5 + trans.amount
            }
        });
        res.render("report", {nameVar: req.user.name, budgetVar: req.user.budget, targetVar: req.user.target, transArr: req.user.transactions,
            dataArr: [
                ['Category', 'Amount(RM)',],
                ['Food', n1],
                ['Social Life', n2],
                ['Transportation', n3],
                ['Education', n4],
                ['Others', n5]
               ], dataDate: today });
    }
});
app.get("/sign_in", function(req, res){
    res.render("sign_in", {});
});

app.post("/rshow",function(req,res){
        var today = req.body.date;
        var tempList = [];
        req.user.transactions.forEach(function(t){
            if(t.date == today){
                tempList.push(t);
            }
        })
        console.log("list "+tempList);
        var n1 = 0,
            n2 = 0,
            n3 = 0,
            n4 = 0,
            n5 = 0;
        tempList.forEach(function(trans){
            console.log("trans.cat : "+trans.categories);
            if (trans.categories == "Food"){
                n1 = n1 + trans.amount; 
            }
            else if(trans.categories == "Social Life"){
                n2 = n2 + trans.amount
            }
            else if(trans.categories == "Transportation"){
                n3 = n3 + trans.amount
            }
            else if(trans.categories == "Education"){
                n4 = n4 + trans.amount
            }
            else {
                n5 = n5 + trans.amount
            }
        });
        res.render("report", {nameVar: req.user.name, budgetVar: req.user.budget, targetVar: req.user.target, transArr: req.user.transactions,
            dataArr: [
                ['Category', 'Amount(RM)',],
                ['Food', n1],
                ['Social Life', n2],
                ['Transportation', n3],
                ['Education', n4],
                ['Others', n5]
               ], dataDate: today });
});

//Transaction router
app.post("/tsave", function(req,res){
    var trans = {
        categories: req.body.categories,
        date: req.body.date,
        amount: req.body.amount,
        remarks: req.body.remarks
    }
    if(req.body.categories != "" &&
        req.body.date != "" &&
        req.body.amount != ""){
            req.user.transactions.push(trans);
            req.user.save();
            req.user.update();
            res.render("manage",{transArr : req.user.transactions});
    }
    else{
        res.redirect("/manage");
    }
})

app.post("/tedit",function(req,res){
    var category, amount, date, remarks;
    var theTran;
    req.user.transactions.forEach(function(t){
        if(t._id == req.body.id){
            theTran = t;
        }
    })
//parse all the value to the webpage
    res.render("manage_edit",{theId : req.body.id, tran : theTran});
});

app.post("/teupdate",function(req,res){
    req.user.transactions.forEach(function(t){
        if(t._id == req.body.id){
            theTran = t;
        }
    })
    console.log("theTran: "+theTran);
    req.user.transactions.remove(req.body.id);
    var trans = {
        categories: req.body.categories,
        date: req.body.date,
        amount: req.body.amount,
        remarks: req.body.remarks
    }
    
    if(req.body.categories == ""){
        trans.categories = theTran.categories;
    }
    if(req.body.date == ""){
        trans.date = theTran.date;
    }
    if(req.body.amount == ""){
        trans.amount = theTran.amount;
    }
    if(req.body.remarks == ""){
        trans.remarks = theTran.remarks;
    }
    console.log("tran: "+trans);
            req.user.transactions.push(trans);
            req.user.save();
            req.user.update();
            res.render("manage",{transArr : req.user.transactions});
})

app.post("/tupdate",function(req,res){
    req.user.transactions.remove(req.body.id);
    var trans = {
        categories: req.body.categories,
        date: req.body.date,
        amount: req.body.amount,
        remarks: req.body.remarks
    }
    if(req.body.categories != "" &&
        req.body.date != "" &&
        req.body.amount != ""){
            req.user.transactions.push(trans);
            req.user.save();
            req.user.update();
            res.render("manage",{transArr : req.user.transactions});
    }
})

app.post("/tdelete", function(req,res){
    req.user.transactions.remove(req.body.id);
    req.user.save();
    req.user.update();
    res.render("manage",{transArr : req.user.transactions});
})

app.post("/back",function(req,res){
    res.redirect("/manage");
})

//post route to handle user sign up
app.post("/register", function(req, res){
    //pass in username and password to a new object, User & create a new user in database
    User.register(new User({username: req.body.username, password: req.body.password, name: req.body.realname, email: req.body.email, age: req.body.age, budget: req.body.budget, target: req.body.target}), req.body.password, function(err, user){
        if(err){
            console.log(err);
            return res.render('sign_in', {}); //go back to sign up page if got error
        }
        //once user been created without error, user will be redirect to the "Secret" page
        //logged the user in using passport
        var userObj;
        passport.authenticate("local")(req, res, function(){
            var mailOptions = {
                from: 'savemo.official@gmail.com',
                to: req.body.email,
                subject: 'SaveMo Registration',
                text: 'Thank you for signing up to SaveMo! \n\n Your details are as follows: \n Username:'+req.body.username+"\n Password"+req.body.password+ '\n We look forward to helping you manage your finances!'
            };

            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });

           res.redirect("/profile_home");
        });
    });
});

// LOGIN ROUTES
//post route to perform authentication
//middleware – having some code that runs between the start of the route and the end of that route handler
//middleware - recalling passport.authenticate to automatically check the username and password in the request
app.post("/login", passport.authenticate("local", {
    successRedirect: "/profile_home",
    failureRedirect: "/sign_in"
}) ,function(req, res){
});

//Route to update data
app.post("/edit", function(req, res){
    //pass in username and password to a new object, User & create a new user in database
    var name, age, email, budget, target;
    name = req.body.editname;
    age = req.body.age;
    email = req.body.email;
    budget = req.body.budget;
    target = req.body.target;
    if(req.body.editname == ""){
        name = req.user.name;
    }
    if(req.body.age == ""){
        age = req.user.age;
    }
    if(req.body.email == ""){
        email = req.user.email;
    }
    if(req.body.budget == ""){
        budget = req.user.budget;
    }
    if(req.body.target == ""){
        target = req.user.target;
    }
    User.updateOne({username: req.user.username},{name: name ,age: age, email: email, budget: budget, target: target}, function(err, user){
        if(err){
            console.log("Oh no, ERROR!");
            console.log(err);
        } else {
            console.log("update successfully")
        }
    });
    res.redirect("/profile");
});

// LOGOUT ROUTES
app.get("/logout", function(req, res){
    req.logout();
    res.redirect("/");
});

// Route for user to recover username and password
app.post("/forgetusername", function(req, res){
    User.findOne({email: req.body.email}, function(err, user){
        if(err){
            console.log("Oh no, ERROR!");
            console.log(err);
        } else {
            var mailOptions = {
                from: 'savemo.official@gmail.com',
                to: req.body.email,
                subject: "SaveMo: Forget Username and Password",
                text: 'This is your username and password: \n Username: '+ user.username+"\n Password: "+ user.password+"\n If you did not request for your username and password, please report to our admin immediately."
            };
            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
        }
    });
    res.redirect("/sign_in");
});

// Route to message savemo
app.post("/message", function(req, res){
    var mailOptions = {
        from: 'savemo.official@gmail.com',
        to: 'savemo.official@gmail.com',
        subject: "Subject: "+ req.body.subject,
        text: 'Details \n Name: '+req.body.realname+"\n Email: "+req.body.email+"\n\n Message: "+req.body.message
    };
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
    res.redirect("/");
});

//Add isLoggedIn middleware 
function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect("/sign_in");
}

// Add the "*" route matcher 
app.get("*", function(req,res){
    res.send("Page Not Found!");
});

app.listen(3000, function(){
   console.log("Server has started!!!");
});
